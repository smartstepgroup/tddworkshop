package com.cft.casino;

//               INTEL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in accordance with the terms of that agreement.
//        Copyright (c) 2013-2014 Intel Corporation. All Rights Reserved.
//
public class Player {
    private Game activeGame;
    private boolean hasChips;

    public void joins(Game game) throws GameJoinException {
        if (this.activeGame != null) {
            throw new GameJoinException("com.cft.casino.Player must leave the current game before joining another game");
        }
        this.activeGame = game;
    }

    public Game activeGame() {
        return activeGame;
    }

    public void leave() {
        activeGame = null;
    }

    public boolean hasChips() {
        return this.hasChips;
    }

    public void buyChips(int chips) {
        hasChips = true;
    }
}
