package com.cft.casino;

import com.cft.casino.Game;
import com.cft.casino.GameJoinException;
import com.cft.casino.PlayerTest;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;

public class WhenPlayGame extends PlayerTest {
    private Game game;

    @Before
    public void setUp() {
        super.setUp();
        game = new Game();
    }

    @Test
    public void playerCanJoinGame() throws GameJoinException {
        player.joins(game);
        assertEquals(game, player.activeGame());
    }

    @Test
    public void playerCanLeaveGame() throws GameJoinException {
        player.joins(game);
        player.leave();

        assertNull(player.activeGame());
    }
}
