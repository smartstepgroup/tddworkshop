package com.cft.casino;

import com.cft.casino.Game;
import com.cft.casino.GameJoinException;
import com.cft.casino.PlayerTest;
import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.fail;

//               INTEL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in accordance with the terms of that agreement.
//        Copyright (c) 2013-2014 Intel Corporation. All Rights Reserved.
//
public class WhenPlayerSwitchesGames extends PlayerTest {
    @Test
    public void playerCanNotJoinTwoGames() throws GameJoinException {
        Game domino = new Game();
        Game chess = new Game();

        player.joins(domino);
        try {
            player.joins(chess);
            fail("Exception com.cft.casino.GameJoinException was not thrown");
        }
        catch (GameJoinException e) {
            assertEquals("com.cft.casino.Player must leave the current game before joining another game", e.getMessage());
        }
    }

    @Test
    public void playerCanJoinAnotherGameAfterLeavingCurrentOne() throws GameJoinException {
        Game domino = new Game();
        Game chess = new Game();

        player.joins(domino);
        player.leave();
        player.joins(chess);

        Assert.assertEquals(chess, player.activeGame());
    }

}
