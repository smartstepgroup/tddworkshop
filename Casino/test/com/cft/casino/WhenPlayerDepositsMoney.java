package com.cft.casino;

import com.cft.casino.PlayerTest;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;

//               INTEL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in accordance with the terms of that agreement.
//        Copyright (c) 2013-2014 Intel Corporation. All Rights Reserved.
//
public class WhenPlayerDepositsMoney extends PlayerTest {
    @Test
    public void newPlayerHasNoChips() {
        assertEquals(false, player.hasChips());
    }

    @Test
    public void playerHasChipsAfterHeBoughtSomeChips() {
        player.buyChips(1);
        assertTrue(player.hasChips());
    }
}
