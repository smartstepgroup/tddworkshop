/**
 * Created by Kursant on 09.04.2014.
 */
public class StringCalculator {

    private String separator = ",";

    public int calculate(String s) {
        if (s.isEmpty()) {
            return 0;
        }

        return recursiveSum(s);
    }

    private int recursiveSum(String s){
        int endIndex = s.indexOf(separator);
        if (endIndex > -1 ) {
            return Integer.parseInt(s.substring(0, endIndex)) + recursiveSum(s.substring(endIndex + 1));
        } else {
            return Integer.parseInt(s);
        }
    }
}
