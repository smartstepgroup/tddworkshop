import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringCalculatorTest {

    private StringCalculator calculator;
    @Before
    public void setUp() {
        calculator = new StringCalculator();
    }

    @Test
    public void emptyStringReturnsZero(){
        int sum = new StringCalculator().calculate("");
        assertEquals(0, sum);
    }

    @Test
    public void singleNumberReturnsNumber(){
        int sum = new StringCalculator().calculate("21");
        assertEquals(21, sum);
    }

    @Test
    public void twoNumbersReturnsSum(){
        int sum = new StringCalculator().calculate("2,1");
        assertEquals(2 + 1, sum);
    }

    @Test
    public void someNumbersReturnsSum() {
        assertEquals(1 + 2 + 3 + 4 + 5, calculator.calculate("1,2,3,4,5"));
    }


}
