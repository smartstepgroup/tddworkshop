package com.cft;

import com.cft.StringCalculator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Kursant on 09.04.2014.
 */
public class StringCalculatorTests {

    private StringCalculator stringCalculator;

    @Before
    public void setUp() {
        stringCalculator = new StringCalculator();
    }

    @Test
    public void emptyStringGives0() {
        int sum = stringCalculator.sum("");
        assertEquals(0, sum);
    }

    @Test
    public void singleNumberGivesThisNumber() {
        int sum = stringCalculator.sum("2112");
        assertEquals(2112, sum);
    }

    @Test
    public void anotherSingleNumberGivesThisNumber() {
        int sum = stringCalculator.sum("1");
        assertEquals(1, sum);
    }

    @Test
    public void twoNumbersGiveSum() {
        int sum = stringCalculator.sum("1,2");
        assertEquals(1 + 2, sum);
    }

    @Test
    public void anotherTwoNumbersGiveSum() {
        int sum = stringCalculator.sum("2,3");
        assertEquals(2+3, sum);
    }
}
