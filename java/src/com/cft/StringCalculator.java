package com.cft;

/**
 * Created by Kursant on 09.04.2014.
 */
public class StringCalculator {

    public static final int DEFAULT_VALUE = 0;
    private String delimiter = ",";

    public int sum(String input) {
        if (input.isEmpty()) {
            return DEFAULT_VALUE;
        }

        if (input.contains(delimiter)) {
            return parseTwoNumbers(input);
        }

        return parseSingleNumber(input);
    }

    private int parseTwoNumbers(String input) {
        int commaPosition = input.indexOf(",");
        String left = input.substring(0, commaPosition);
        String right = input.substring(commaPosition + 1);
        return parseSingleNumber(left) + parseSingleNumber(right);
    }

    private Integer parseSingleNumber(String input) {
        return Integer.valueOf(input);
    }
}
